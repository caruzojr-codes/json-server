# Json-Server e Mock de API
Como trabalhar com um mock de api utilizando o json-server



## Run Project
Clone este repositório para a máquina local
```
git clone https://caruzojr@bitbucket.org/caruzojr/json-server.git
```
Dentro da pasta do projeto, execute o comando abaixo para baixar todas as dependencias necessárias
```
npm install
```
Rode o seguinte comando para executar o projeto
```
ng s --open
```
Para rodar o mock do json-serve, abra um novo terminal, e dentro da pasta `dbmock`, rode o seguinte comando
```
json-server --watch db.json
```
