import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from "rxjs/operators";

import { Filme } from './filme.model';

@Injectable({
  providedIn: 'root'
})
export class FilmeService {

  private readonly urlAPI = "http://localhost:3000";

  constructor(
    private httpClient: HttpClient
  ) { }

  getAllFilmes(): Observable<Filme[]> {
    return this.httpClient.get<Filme[]>(
      this.urlAPI + `/filmes`
    ).pipe(
      catchError(error => {
        return throwError(Object.keys(error));
      })
    );
  }

}
