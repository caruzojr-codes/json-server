import { Component, OnInit } from '@angular/core';

import { FilmeService } from './filme.service';
import { Filme } from './filme.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  filmes: Filme[] = [];

  constructor(
    private filmeService: FilmeService
  ) {}

  ngOnInit(): void {
    this.getAllFilmes();
  }

  getAllFilmes() {
    this.filmeService.getAllFilmes()
    .subscribe(retorno => {
      this.filmes = retorno;
    },
    error => console.error(error)
    );
  }
}
